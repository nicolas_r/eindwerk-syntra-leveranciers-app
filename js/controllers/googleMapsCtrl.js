app.controller("googleMapsCtrl", function($scope, $http, ordersFactory, $window){

    /****************************************************************************************************************
     * Global variables ...
     ***************************************************************************************************************/
    var _adresRestaurant = null;
    var _adresEindKlant = null;
    var _order = null;
    var _intervalId = null;
    var _intervalId2 = null;
    var _checkOrderInterval = null;

    $scope.claimBtn = true; // Hidden ?
    $scope.pickupBtn = true; // Hidden ?
    $scope.hideRoute2 = true;
    $scope.deliverBtn = true;

    var disableF5 = function () {
        console.log("trying to disable F5");
        document.onkeydown = function (e) {
            return (e.which || e.keyCode) != 116;
        };
    };

    // LOCAL STORAGE / SESSION STORAGE
    // $window bewaren in een 'local storage' voor page refresh...
    // Popup route onthoudt nl. de geinjecteerde parameter niet na page-refresh...
    // Check browser support

    if (typeof(Storage) !== "undefined") {
        //-- Check if sessionStorage has existing value of adres
        if(sessionStorage.order) {
            _order = JSON.parse(sessionStorage.getItem('order'));
        } else {
            sessionStorage.setItem('order', JSON.stringify($window.data.order));
            _order = $window.data.order;  // test - Order object (in z'n geheel)
        }
    } else {
        // Sorry! No Web Storage support in your browser..
        alert("This app may not work properly, sessionStorage not supported!");
    }

    $scope.restaurant = _order.restaurant;
    $scope.klant = _order.user;
    $scope.order = _order;
    $scope.leveranciers = _order.leveranciers;

    //-- Angular-google-maps vereist minstens een 'center' or 'bounds-property' - Default settings:
    $scope.map = {
        center: {
            latitude: 51,
            longitude: 4
        },
        zoom: 15,
        options: {
            draggable: true
        },
        control: {}
    };

    //-- Array die alle markers zal bewaren
    $scope.markers = [];

    //-- Routebeschrijving van -> naar ...
    var _adressen = {
        van: "spoorwegstraat 14, 8000 Brugge",
        naar: _adresRestaurant,
        showList: false
    };

    //-- Om het te kunnen weergeven op je kaart:
    var directionsDisplay = new google.maps.DirectionsRenderer();

    //-- Je hebt een 'directions service' nodig (om de route te kunnen berekenen)
    var directionsService = new google.maps.DirectionsService();

    //-- Zoek geo-locatie op op basis van ingegeven (of doorgegeven) adres
    var geocoder = new google.maps.Geocoder();

    /****************************************************************************************************************
     * Initialiseren - Vind de locatie van het 'doorgegeven' adres
     ***************************************************************************************************************/
    var init = function(){
        var naamRestaurant = _order.restaurant.naam_restaurant;
        DEBUG ? console.log("NAAM RESTAURANT: " + naamRestaurant) : '';
        _adresRestaurant = parseAddres(_order.restaurant);
        _adresEindKlant = parseAddres(_order.user);
        DEBUG ? console.log("_adresRestaurant & _adresEindKlant data...") : '';
        DEBUG ? console.log(_adresRestaurant) : '';
        DEBUG ? console.log(_adresEindKlant) : '';

        //-- Route Current Location -> Restaurant
        _adressen.naar = _adresRestaurant;
        getCurrentLocationAndDestination(_adresRestaurant);

        // DEBUG ? console.log(_order.order.orderTime) : '';
        var orderTime = convertTimestampToTime(_order.bestellingtijdstip);
        $scope.order.orderTime = orderTime;
        var pickupTime = convertTimestampToTime(_order.bestellingtijdstip);
        $scope.order.pickupTime = pickupTime;

    };

    /****************************************************************************************************************
     * Parst het adres object naar een string
     * @param addrObject
     * @returns {string}
     ***************************************************************************************************************/
    var parseAddres = function(addrObject){
        var adress = addrObject.straat + ' ' + addrObject.nr + ', ' + addrObject.postcode + ' ' + addrObject.gemeente;
        return adress;
    };

    /****************************************************************************************************************
     * ALGEMENE FUNCTIE (herbruikbaar)
     * Routeberekening op basis van gegevens in het interne object adressen
     ***************************************************************************************************************/
    $scope.getDirections = function(){
        // DEBUG ? console.log("Routeberekening") : '';
        // DEBUG ? console.log($scope.map) : '';

        var travelMode = document.querySelector('#mode').value;

        var parameters = {
            origin: _adressen.van,
            destination: _adressen.naar,
            travelMode: travelMode
        };

        $scope.claimBtn = false;
        DEBUG ? console.log(parameters) : '';

        directionsService.route(parameters, function (response, status) {
            directionsDisplay.setDirections(response);
            directionsDisplay.setMap($scope.map.control.getGMap());
            directionsDisplay.setPanel(document.querySelector('#route'));
        });
    };

    /****************************************************************************************************************
     * todo: AJAX call naar server om status order te wijzigen + order te claimen
     ***************************************************************************************************************/
    $scope.claimSelectedOrder = function(){
        DEBUG ? alert("Order wordt geclaimd!") : '';

        disableF5();

        //
        var putData = {
            id: _order.id,
            order: _order
        };
        console.log(putData);

        //console.log(ordersFactory.claimOrder(order));
        ordersFactory.claimOrder(putData).then(function (response) {
            console.log(response)

            if(response.data.result.bestellingstatusen_id > 2 ){
                console.log("showing button");
                $scope.pickupBtn = false; // Not hidden...
            }

            // if (typeof response.data.errors == "undefined") {
            //
            //     //console.log(response)
            //     //console.log("PUT command is gelukt!  Volgende data is aangepast:");
            //     //console.log(response.data);
            //
            // } else {
            //     //validateServerData(response.data.errors);
            // }
        });

        $scope.cancelBtn = true;  // hidden...
        //$scope.pickupBtn = false; // Not hidden...
        var button = document.querySelector('#claimBtn');
        button.disabled = true;
        button.innerHTML = "Claim Confirmed!";

        //checkBestelling(putData);
        _checkOrderInterval = setInterval(function(){checkBestelling(putData); }, 5000);
    };

    var checkBestelling = function(order){
        //console.log(ordersFactory.claimOrder(order));
        ordersFactory.checkOrder(order).then(function (response) {
            console.log("Check besteling...");
            console.log(response);

            if(response.data.result.bestellingstatusen_id > 2 ){
                $scope.pickupBtn = false; // Not hidden...
            } else {
                console.log("Hiding button");
                $scope.pickupBtn = true; // Hidden...
            }

            // if (typeof response.data.errors == "undefined") {
            //
            //     //console.log(response)
            //     //console.log("PUT command is gelukt!  Volgende data is aangepast:");
            //     //console.log(response.data);
            //
            // } else {
            //     //validateServerData(response.data.errors);
            // }
        });

    };

    $scope.cancelAndClose = function(){
        console.log("closing window");
        window.close();
    };

    /****************************************************************************************************************
     * Bereken route van restaurant naar klant
     * todo: * + AJAX call naar server om status order te wijzigen naar "onderweg" + approx reistijd doorgeven
     *
     * Hier kan je een 'timer' starten die alle 30 seconden de geolocatie de resterende afstand van het traject
     * doorgeeft aan de server...
     ***************************************************************************************************************/
    $scope.pickUpOrder = function(){
        DEBUG ? alert("Order is opgepikt!") : '';
        var button = document.querySelector('#pickupBtn');
        button.disabled = true;
        $scope.deliverBtn = false;
        button.innerHTML = "Pick-up Confirmed!";

        DEBUG ? console.log("ROUTE NAAR EINDKLANT: " + _adresEindKlant) : '';

        var time = Date.now() / 1000;
        console.log(time);

        var putData = {
            id: _order.id,
            status: 4,
            pickup_time: time
        };

        //console.log(ordersFactory.claimOrder(order));
        ordersFactory.changeStatus(putData).then(function (response) {
            console.log(response)

            // if (typeof response.data.errors == "undefined") {
            //
            //     //console.log(response)
            //     //console.log("PUT command is gelukt!  Volgende data is aangepast:");
            //     //console.log(response.data);
            //
            // } else {
            //     //validateServerData(response.data.errors);
            // }
        });

        //-- Route Current Location -> EindKlant
        _adressen.naar = _adresEindKlant;
        getCurrentLocationAndDestination(_adresEindKlant);
        _intervalId2 = setInterval(getCurrentLocationAndDestination(_adresEindKlant), 20000);

        getTimeToDestination();
        _intervalId1 = setInterval(getTimeToDestination, 30000);

        clearInterval(_checkOrderInterval);
    };

    /****************************************************************************************************************
     * Order is integraal geleverd bij de klant...                                                                  *
     ***************************************************************************************************************/
    $scope.deliverOrder = function(){
        DEBUG ? alert("Order is geleverd!") : '';
        var button = document.querySelector('#deliverBtn');
        button.disabled = true;
        button.innerHTML = "Delivery Confirmed!";

        var time = Date.now() / 1000;
        console.log(time);

        var putData = {
            id: _order.id,
            status: 6,
            delivery_time: time
        };

        //console.log(ordersFactory.claimOrder(order));
        ordersFactory.changeStatus(putData).then(function (response) {
            console.log("delivery response");
            console.log(response)

            // if (typeof response.data.errors == "undefined") {
            //
            //     //console.log(response)
            //     //console.log("PUT command is gelukt!  Volgende data is aangepast:");
            //     //console.log(response.data);
            //
            // } else {
            //     //validateServerData(response.data.errors);
            // }
        });

        clearInterval(_intervalId1);
        clearInterval(_intervalId2);
    };

    /****************************************************************************************************************
     * Wordt all 30 seconden aangeroepen, zodra het traject Restaurant -> klant is begonnen
     * Elke aanroep, pusht de resterende tijd / afstand naar de server...
     ***************************************************************************************************************/
    var getTimeToDestination = function(){

        var travelMode = document.querySelector('#mode').value;

        var parameters = {
            origin: _adressen.van,
            destination: _adressen.naar,
            travelMode: travelMode
        };

        directionsService.route(parameters, function (response, status) {
            // DEBUG ? console.log(response.routes[0].legs[0].duration.text) : '';
            // DEBUG ? console.log(response.routes[0].legs[0].distance.text) : '';

            //-- Todo: Remaining Traveltime --> doorsturen naar de server !!
            console.log(response.routes[0].legs[0].duration.value); // in seconden
            console.log(response.routes[0].legs[0].duration.text);

            var postData = {
                id: _order.id,
                travel_time: response.routes[0].legs[0].duration.value
            };

            // Het response kan vanaf deze fase worden bepaald, op de server wat de leveringstijd wordt...
            ordersFactory.sendTravelTime(postData).then(function(response){
                console.log(response);


            });
        });
    };

    /****************************************************************************************************************
     * Converts timestamp to time (24h format) - Timestamp werkt in seconden (kan je makkelijk tijd uitrekenen)
     * @param unix_timestamp
     * @returns {string}
     ***************************************************************************************************************/
    var convertTimestampToTime = function(unix_timestamp){
        // Create a new JavaScript Date object based on the timestamp
        // multiplied by 1000 so that the argument is in milliseconds, not seconds.
        var date = new Date(unix_timestamp*1000);
        // Hours part from the timestamp
        var hours = date.getHours();
        // Minutes part from the timestamp
        var minutes = "0" + date.getMinutes();
        // Seconds part from the timestamp
        var seconds = "0" + date.getSeconds();

        // Will display time in 10:30:23 format
        var formattedTime = hours + ':' + minutes.substr(-2); // + ':' + seconds.substr(-2);  // HH:MM (SS niet nodig)

        return formattedTime;
    };

    /****************************************************************************************************************
     * Toon de bestemming op via reverse-geocoding op basis van de doorgegeven 'global' variabele 'adres'
     * Zet een pin op de bestemming van het doorgegeven parameter:
     * @param adress
     ***************************************************************************************************************/
    var getDestination = function(adress) {
        geocoder.geocode( {"address": adress}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK && results.length > 0) {
                DEBUG ? console.log("GET DESTINATION LOCATION") : '';
                DEBUG ? console.log(adress) : '';
                var huidigeLocatie = results[0].geometry.location,
                    lat = huidigeLocatie.lat(),
                    lng = huidigeLocatie.lng();

                //-- Pas coordinaten aan
                $scope.map.center.latitude = parseFloat(lat);
                $scope.map.center.longitude = parseFloat(lng);
                $scope.$apply();  //-- NIET VERGETEN!! Anders verandert er niets kwa bovenste wijziging...
                DEBUG ? console.log("DESTINATION LOCATION FOUND: ") : '';
                DEBUG ? console.log($scope.map) : '';

                latlng = {
                    lat: parseFloat(lat),
                    lng: parseFloat(lng)
                };

                //-- Zet eerste marker op de 'gevonden' startcoordinaten
                $scope.markers.push({
                    idKey: $scope.markers.length + 1,
                    coords: {
                        latitude: lat,
                        longitude: lng
                    },
                    options: {
                        draggable: false,
                        labelContent: adress,
                        labelAnchor: "100 0",       // positionering van de marker ?!
                        labelClass: "marker-labels", // CSS class voor deze labels (kan je zelf veranderen in stylesheet)
                        opacity: 1.0
                        // label: "TEST"    // Labelcontent verdwijnt als je label gebruikt...
                        // animation: google.maps.Animation.DROP  // werkt niet
                    }
                });
            }
        });
    };

    /****************************************************************************************************************
     * Haal huidige EIGEN geo-locatie op, bewaar deze automatisch in het global 'vertrek-adres' object: _adressen.van
     * Indien eigen geo-locatie gevonden, zoek dan pas de bestemming op...
     *
     * @param adress : wordt doorgegeven voor opzoeken bestemming. Door async call kan dit enkel zo...?
     ***************************************************************************************************************/
    var getCurrentLocationAndDestination = function(adress) { // Knop verwijderd
        DEBUG ? console.log("Geolocation - GET CURRENT LOCATION") : '';

        //-- Staat geolocatie aan?
        if ("geolocation" in navigator) {

            //-- Zoek eerst je eigen geo-locatie op (waar je je momenteel bevindt)
            //   Heb je nodig om route te rekenen
            //-- De ontvangende code van de callback wordt in de variable position opgevangen
            navigator.geolocation.getCurrentPosition(function (position) {
                $scope.map.center.latitude = position.coords.latitude;
                $scope.map.center.longitude = position.coords.longitude;
                $scope.$apply();  // NIET VERETEN - Belangrijk !!

                var latlng = {
                    lat: parseFloat(position.coords.latitude),
                    lng: parseFloat(position.coords.longitude)
                };

                DEBUG ? console.log("CURRENT LOCATION FOUND: coordinaten voor reverse geolocatie zijn: ") : '';
                DEBUG ? console.log(latlng) : '';

                // De coordinaten die worden teruggeven zijn een 'promise', alles moet gebeuren
                // binnen de de closure van getCurrentPosition, belangrijk!!
                // Doe nu op basis van deze data een "reverse geo coding" om coordinates om
                // te zetten naar een adres:
                geocoder.geocode({'location': latlng}, function(results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            //DEBUG ? console.log(results) : '';
                            _adressen.van = results[0].formatted_address;

                            //-- Zet tweede marker op de 'gevonden' doel-coordinaten
                            $scope.markers.push({
                                idKey: $scope.markers.length + 1,
                                coords: {
                                    latitude: latlng.lat,
                                    longitude: latlng.lng
                                }
                            });

                            DEBUG ? console.log("CURRENT LOCATION: Vertrek-coordinaten zijn als volgt: ") : '';
                            DEBUG ? console.log("CURRENT LOCATION: lat: " + latlng.lat) : '';
                            DEBUG ? console.log("CURRENT LOCATION: lat: " + latlng.lng) : '';
                            DEBUG ? console.log("CURRENT LOCATION: Gevonden adres na reverse geo-coding:") : '';
                            DEBUG ? console.log(_adressen.van) : '';
                            //infowindow.setContent(results[1].formatted_address);
                            //infowindow.open(map, marker);

                            //-- Zoek bestemming pas, NADAT je eigen geo-locatie is bepaald...
                            getDestination(adress);

                        } else {
                            window.alert('Geen bestemming gevonden, kan geen route berekenen!');
                        }

                    } else {
                        window.alert('Geolocatie niet beschikbaar, kan geen route berekenen!  Fout: ' + status);
                    }
                });
            });

            // Bovenstaande functies zijn callbacks.  Voert de functie uit als hij klaar is
            // Daarom dat de bestemming pas wordt gezocht, nadat de currentLocation is gekend!

        } else {
            console.alert("Geolocatie niet beschikbaar, kan geen route berekenen!");
        }
    };

    init();
});
