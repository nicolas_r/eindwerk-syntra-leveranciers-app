/**
 * Created by nrt on 30/03/2017.
 */
app.controller('orderCtrl', function($scope, ordersFactory, $window){
    $scope.orders = [];

    var _getServerData = function(){
        console.log("Haal orders op:");
        ordersFactory.getOrders().then(function(response){

            console.log(response);
            $scope.orders = response.data.result;
            console.log($scope.orders);
        });
    };

    var init = function(){
        _getServerData();
        setInterval(_getServerData, 10000);
    };

    /**
     * Todo: Pagina alle 30 seconden refreshen, om de 'geclaimde' orders eruit te filteren indien nodig...
     * Todo: Indien iemand anders op dit order klikt, nogmaals checken op de server of deze al geclaimd is of niet
     */
    $scope.locatieBtn = function(index) {

        // todo: ajax call, checken op id of dit order reeds geclaimd is of niet
        // claimed = ... (return true / false van xhr call (laravel)

        // if(claimed){
        //     // geef foutmelding
        //     // refresh pagina
        // } else {
        //
        // }

        var $popup = $window.open("#!/location/", "popup", "width=1000, height=1000");
        // $popup = adres;  //-- Doorgeven adres aan pop-up window  (id is niet nodig)
        $popup.data = {
            // adres: adres,
            order: $scope.orders[index]
        };  //-- Doorgeven adres aan pop-up window  (id is niet nodig)

        console.log($popup.data);
    };

    init();
});