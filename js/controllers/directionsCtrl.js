/**
 * Created by nrt on 30/03/2017.
 */
app.controller('directionsCtrl', function($scope, $http){

    //-- Angular-google-maps vereist minstens een 'center' or 'bounds-property' - Default settings:
    $scope.map = {
        center: {
            latitude: 51,
            longitude: 4
        },
        zoom: 15,
        options: {
            draggable: true
        },
        control: {}
    };

    // Routebeschrijving van -> naar ...
    var adressen = {
        van: "kreekstraat, oostende",
        naar: "Frituur Bosrand"
    };

    // Je hebt een 'directions service' nodig (om de route te kunnen berekenen)
    var directionsService = new google.maps.DirectionsService();

    // Om het te kunnen weergeven op je kaart:
    var directionsDisplay = new google.maps.DirectionsRenderer();

    // dit is een callback. Voert de functie uit als hij klaar is
    // response is hier waar je route in zit
    // status is...
    var parameters = {
        origin: adressen.van,
        destination: adressen.naar,
        travelMode: "BICYCLING"
    };

    directionsService.route(parameters, function(response, status){
        console.log(response); // Bevat handige info zoals reistijd, afstand, etc.
        directionsDisplay.setDirections(response);
        directionsDisplay.setMap($scope.map.control.getGMap());
        directionsDisplay.setPanel(document.querySelector('#route'));
    });

    $scope.getDirections = function() {

    };
});