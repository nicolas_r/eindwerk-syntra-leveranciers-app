var app = angular.module("googleMapsApp", ['uiGmapgoogle-maps', 'ngRoute']);

const DEBUG = false;
DEBUG ? console.log("DEBUG ENABLED") : console.log("DEBUG DISABLED");

app.config(function($routeProvider){
   $routeProvider.when('/', {
        templateUrl: 'js/templates/orders.html',
        controller: 'orderCtrl'
    }).when('/directions', {
        templateUrl: "js/templates/directions.html",
        controller: 'directionsCtrl'
    }).when('/location', {
        templateUrl: "js/templates/location.html",
        controller: 'googleMapsCtrl'
    }).when('/plunkertest', {
        templateUrl: "js/templates/plunker.html",
        controller: 'plunkerCtrl'
    });
});
