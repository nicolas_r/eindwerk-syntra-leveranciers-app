/**
 * Created by nrt on 11/04/2017.
 */
app.factory("ordersFactory", function($http){
    var factory = {};
    //var api = "//food4u.smoetje.be/api/overzicht";
    //var api = "http://eindwerk.syntra/api/overzicht";

    switch (window.location.host) {
        case "eindwerk.leveranciers.syntra":
            var api = "http://eindwerk.syntra/api/overzicht";
            break;

        case "food4u.smoetje.be":
            var api = "//food4u.smoetje.be/api/overzicht";
            break;

        case "leveranciers.smoetje.be":
            var api = "//food4u.smoetje.be/api/overzicht";
            break;

        case "leveranciers.syntra":
            var api = "//food4u.smoetje.be/api/overzicht";
            break;

        default:
            alert("App draait op niet-geregistreerde URL. Check factory!");
    }

    factory.getOrders = function(){
        return $http.get(api);
    };

    //-- Gewoon testje, ophalen 1 specifiek order...
    factory.getOrder = function(orderNr){
        var order = $http.get(api);
        return order;
    };

    factory.claimOrder = function(order){
        return $http.put(api + "/claim/" + order.id, order);
    };


    factory.checkOrder = function(order){
        return $http.get(api + "/order/" + order.id);
    };

    factory.changeStatus = function(order){
        return $http.put(api + "/leveringsstatus/" + order.id, order);
    };

    factory.sendTravelTime = function(order){
        return $http.post(api + "/traveltime/" + order.id, order);
    };

    return factory;
});